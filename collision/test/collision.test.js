/**
 * Created with JetBrains WebStorm.
 * User: Nitesh
 * Date: 30/03/13
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */

describe("collision tests", function () {


    it("Collision with 'a' moving 'right'", function () {

        var a = {origin: {xPos: 0, yPos: 0}, width: 20, height: 10};
        var b = {origin: {xPos: 30, yPos: 0}, width: 20, height: 10};
        var delta = {xPos: 20, yPos: 0};

        var expected = {xPos: 10, yPos: 0, isCollision: true};
        var actual = collision.getCollision(a, b, delta);

        expect(expected).toEqual(actual);
    });

    it("Collision with 'a' moving 'right/down'", function () {

        var a = {origin: {xPos: 0, yPos: 0}, width: 20, height: 10};
        var b = {origin: {xPos: 30, yPos: 20}, width: 20, height: 10};
        var delta = {xPos: 20, yPos: 40};

        var expected = {xPos: 10, yPos: 20, isCollision: true};
        var actual = collision.getCollision(a, b, delta);

        expect(expected).toEqual(actual);
    });

    it("Collision with 'a' moving 'left'", function () {

        var a = {origin: {xPos: 30, yPos: 0}, width: 20, height: 10};
        var b = {origin: {xPos: 0, yPos: 0}, width: 20, height: 10};
        var delta = {xPos: -20, yPos: 0};

        var expected = {xPos: -10, yPos: 0, isCollision: true};
        var actual = collision.getCollision(a, b, delta);

        expect(expected).toEqual(actual);
    });

    it("Collision with 'a' moving 'up'", function () {

        var a = {origin: {xPos: 0, yPos: 30}, width: 20, height: 10};
        var b = {origin: {xPos: 0, yPos: 0}, width: 20, height: 10};
        var delta = {xPos: 0, yPos: -30};

        var expected = {xPos: 0, yPos: -20, isCollision: true};
        var actual = collision.getCollision(a, b, delta);

        expect(expected).toEqual(actual);
    });

    it("Collision with 'a' moving 'down'", function () {

        var a = {origin: {xPos: 0, yPos: 0}, width: 20, height: 10};
        var b = {origin: {xPos: 0, yPos: 30}, width: 20, height: 10};
        var delta = {xPos: 0, yPos: 30};

        var expected = {xPos: 0, yPos: 20, isCollision: true};
        var actual = collision.getCollision(a, b, delta);

        expect(expected).toEqual(actual);
    });
});
