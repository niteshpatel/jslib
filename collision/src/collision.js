var collision;
(function (collision) {
    function getCollision(a, b, delta) {
        var xCollision = 0;
        var yCollision = 0;
        var xProportion = 1;
        var yProportion = 1;
        var deltaProportion = 1;
        var isCollision = false;
        if(a.origin.xPos + a.width > b.origin.xPos) {
            xCollision = Math.min((a.origin.xPos + delta.xPos) - (b.origin.xPos + b.width), 0);
        } else if(a.origin.xPos < b.origin.xPos + b.width) {
            xCollision = Math.max((a.origin.xPos + a.width + delta.xPos) - b.origin.xPos, 0);
        }
        if(a.origin.yPos + a.height > b.origin.yPos) {
            yCollision = Math.min((a.origin.yPos + delta.yPos) - (b.origin.yPos + b.height), 0);
        } else if(a.origin.yPos < b.origin.yPos + b.height) {
            yCollision = Math.max((a.origin.yPos + a.height + delta.yPos) - b.origin.yPos, 0);
        }
        if(xCollision && yCollision) {
            xProportion = 0;
            yProportion = 0;
            if(delta.xPos) {
                xProportion = (delta.xPos - xCollision) / delta.xPos;
            }
            if(delta.yPos) {
                yProportion = (delta.yPos - yCollision) / delta.yPos;
            }
            deltaProportion = Math.max(xProportion, yProportion);
            isCollision = true;
        }
        return {
            xPos: delta.xPos * deltaProportion,
            yPos: delta.yPos * deltaProportion,
            isCollision: isCollision
        };
    }
    collision.getCollision = getCollision;
})(collision || (collision = {}));
//@ sourceMappingURL=collision.js.map
