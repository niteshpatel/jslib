/**
 * Created with JetBrains WebStorm.
 * User: Nitesh
 * Date: 30/03/13
 * Time: 13:36
 * To change this template use File | Settings | File Templates.
 */

module collision {

    export interface Coordinates {
        xPos:number;
        yPos:number;
    }

    export interface Rectangle {
        origin: Coordinates;
        width:number;
        height:number;
    }

    export function getCollision(a:Rectangle, b:Rectangle, delta:Coordinates):Coordinates {

        var xCollision:number = 0;
        var yCollision:number = 0;
        var xProportion:number = 1;
        var yProportion:number = 1;
        var deltaProportion:number = 1;
        var isCollision = false;

        // Left collision
        if (a.origin.xPos + a.width > b.origin.xPos) {
            xCollision = Math.min((a.origin.xPos + delta.xPos) - (b.origin.xPos + b.width), 0);
        }
        // Right collision
        else if (a.origin.xPos < b.origin.xPos + b.width) {
            xCollision = Math.max((a.origin.xPos + a.width + delta.xPos) - b.origin.xPos, 0);
        }
        // Top collision
        if (a.origin.yPos + a.height > b.origin.yPos) {
            yCollision = Math.min((a.origin.yPos + delta.yPos) - (b.origin.yPos + b.height), 0);
        }
        // Bottom collision
        else if (a.origin.yPos < b.origin.yPos + b.height) {
            yCollision = Math.max((a.origin.yPos + a.height + delta.yPos) - b.origin.yPos, 0);
        }

        // If we had a collision, work out how much we can actually move
        if (xCollision && yCollision) {
            xProportion = 0;
            yProportion = 0;
            if (delta.xPos) {
                xProportion = (delta.xPos - xCollision) / delta.xPos;
            }
            if (delta.yPos) {
                yProportion = (delta.yPos - yCollision) / delta.yPos;
            }
            deltaProportion = Math.max(xProportion, yProportion);
            isCollision = true;
        }

        // If both xCollision and yCollision are zero we have no collision
        return {
            xPos: delta.xPos * deltaProportion,
            yPos: delta.yPos * deltaProportion,
            isCollision: isCollision}
    }
}
